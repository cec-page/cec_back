import mongoose, { Schema, Document } from "mongoose";

export interface IUniversity extends Document {
  name: string;
  description: string;
  slug: string;
  departments: string[];
  subjects: string[];
}

const universitySchema = new Schema({
  name: { type: String, required: true, trim: true },
  description: { type: String, required: true, trim: true },
  slug: { type: String, unique: true, required: true, trim: true },
  departments: {
    type: [Schema.Types.ObjectId],
    ref: "Department",
    required: false,
    default: [],
  },
  subjects: {
    type: [Schema.Types.ObjectId],
    ref: "Subject",
    required: false,
    default: [],
  },
});

export const University = mongoose.model<IUniversity>(
  "University",
  universitySchema
);

export default University;
