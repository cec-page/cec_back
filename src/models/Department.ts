import mongoose, { Schema, Document } from "mongoose";

export interface IDepartment extends Document {
  name: string;
  description: string;
  subjects: string[];
  universityId: string;
  slug: string;
}

export const departmentSchema: Schema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  subjects: [
    {
      type: Schema.Types.ObjectId,
      ref: "Subject",
      required: false,
      default: [],
    },
  ],
  universityId: {
    type: Schema.Types.ObjectId,
    ref: "University",
    required: true,
  },
  slug: { type: String, unique: true, required: true },
});

export const Department = mongoose.model<IDepartment>(
  "Department",
  departmentSchema
);

export default Department;
