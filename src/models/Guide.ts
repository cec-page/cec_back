import mongoose, { Schema, Document } from "mongoose";
import * as Yup from "yup";

enum GuideType {
  Examen = "examen",
  Guía = "guía",
  Chuleta = "chuleta",
  Práctica = "práctica",
}

enum GuideState {
  PendingForApproval = "pendiente por aprobación",
  Published = "publicada",
  Rechazada = "rechazada",
}

export interface IGuide extends Document {
  name: string;
  type: GuideType;
  semester: string;
  author: string;
  rating: number;
  description: string;
  uploadDate: Date;
  reviewedBy?: string;
  uploadedBy: string;
  reviewNote?: string;
}

export const guideTypeValidator = Yup.string().oneOf([
  ...Object.values(GuideType),
]);

const GuidesSchema = new Schema<IGuide>({
  name: { type: String, required: true, trim: true },
  type: { type: String, enum: [...Object.values(GuideType)], required: true },
  semester: { type: String, required: true },
  author: { type: String, required: true },
  rating: { type: Number, required: true },
  description: { type: String, required: true },
  uploadDate: { type: Date, required: true },
  state: {
    type: String,
    enum: [...Object.values(GuideState)],
    required: true,
    default: GuideState.PendingForApproval,
  },
  reviewedBy: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: false,
  },
  uploadedBy: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  reviewNote: {
    type: String,
    required: function (this: IGuide) {
      return this.reviewedBy;
    },
  },
});

const Guide = mongoose.model<IGuide>("Guides", GuidesSchema);

export default Guide;
