import mongoose, { Document, Schema } from "mongoose";
import { hashPassword } from "../util/auth";

export interface IUser extends Document {
  name: string;
  lastname: string;
  password: string;
  email: string;
  userName: string;
  belongsToDepartment: string;
}

const userSchema: Schema = new Schema({
  name: { type: String, required: true, trim: true },
  lastname: { type: String, required: true, trim: true },
  password: { type: String, required: true },
  userName: { type: String, required: true, unique: true, trim: true },
  email: { type: String, required: true, trim: true },
  belongsToDepartment: {
    type: Schema.Types.ObjectId,
    ref: "Department",
    required: false,
  },
});

userSchema.pre<IUser>("save", function (next) {
  const user = this;
  if (!user.isModified("password")) return next();
  hashPassword(user.password)
    .then((hash) => {
      user.password = hash;
      next();
    })
    .catch(next);
});

const User = mongoose.model<IUser>("User", userSchema);

export default User;
