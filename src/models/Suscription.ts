import mongoose, { Schema, Document, Types } from "mongoose";
import { Request, Response, NextFunction } from "express";
import { expressYupMiddleware } from "express-yup-middleware";
import * as Yup from "yup";
import { StatusCodes, statusCodeToReasonPhrase } from "http-status-codes";
import NotFoundError from "../exception/NotFoundError";

export interface ISuscription extends Document {
  user: string;
  subject: string;
}

const suscriptionSchema: Schema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  subject: { type: Schema.Types.ObjectId, ref: "Subject", required: true },
});

const Suscription = mongoose.model<ISuscription>(
  "Suscription",
  suscriptionSchema
);

export const ToggleSuscription = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            userId: Yup.string().required(),
            subjectId: Yup.string().required(),
          }),
        },
      },
    },
  }),
  async (
    req: Request<{ userId: string; subjectId: string }>,
    res: Response,
    next: NextFunction
  ) => {
    const { userId, subjectId } = req.params;
    const existingSuscription = await Suscription.findOne({
      user: userId,
      subject: subjectId,
    });

    if (existingSuscription) {
      Suscription.deleteOne({
        user: req.params.userId,
        subject: req.params.subjectId,
      })
        .exec()
        .then((suscription) => {
          return res.status(StatusCodes.OK).json({
            suscription,
            isSuscribed: false,
          });
        })
        .catch(next);

      res.status(StatusCodes.OK).json();
      return;
    } else {
      (
        await Suscription.create({
          user: req.params.userId,
          subject: req.params.subjectId,
        })
      )
        .execPopulate()
        .then((suscription) => {
          return res
            .status(StatusCodes.CREATED)
            .json({ suscription, isSuscribed: true });
        })
        .catch(next);
    }
  },
];

export const UsersBySubject = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            subjectId: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ subjectId: string }>, res: Response, next: NextFunction) => {
    Suscription.find({ subject: req.params.subjectId })
      .exec()
      .then((suscription) => {
        if (!suscription) {
          next(new NotFoundError("Id invalido"));
          return;
        }
        return res.status(StatusCodes.OK).json(suscription);
      })
      .catch(next);
  },
];

export const SubjectsByUser = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            userId: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ userId: string }>, res: Response, next: NextFunction) => {
    Suscription.find({ user: req.params.userId })
      .exec()
      .then((subject) => {
        return res.status(StatusCodes.OK).json(subject);
      })
      .catch(next);
  },
];

export const CheckIfUserIsSuscribed = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            userId: Yup.string().required(),
            subjectId: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (
    req: Request<{ userId: string; subjectId: string }>,
    res: Response,
    next: NextFunction
  ) => {
    Suscription.findOne({
      user: req.params.userId,
      subject: req.params.subjectId,
    })
      .exec()
      .then((suscription) => {
        if (!suscription) {
          res.status(StatusCodes.OK).json(false);
        }
        return res.status(StatusCodes.OK).json(true);
      })
      .catch(next);
  },
];

export default Suscription;
