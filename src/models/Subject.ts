import mongoose, { Schema, Document } from "mongoose";

export interface ISubject extends Document {
  name: string;
  description: string;
  departmentId: string;
  guides: string[];
  slug: string;
  university: string;
}

const subjectSchema: Schema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  departmentId: {
    type: Schema.Types.ObjectId,
    ref: "Department",
    required: true,
  },
  university: {
    type: Schema.Types.ObjectId,
    ref: "University",
    required: false,
  },
  guides: [
    {
      type: Schema.Types.ObjectId,
      ref: "Guides",
      required: false,
      default: [],
    },
  ],
  slug: { type: String, required: true, unique: true },
});

export const Subject = mongoose.model<ISubject>("Subject", subjectSchema);

export default Subject;
