import { EnvType, load } from "ts-dotenv";

export type Env = EnvType<typeof schema>;

export const schema = {
  NODE_ENV: ["production" as const, "development" as const],
  PORT: Number,
  JWT_ID_DURATION: Number,
  JWT_REFRESH_DURATION: Number,
  JWT_SECRET: String,
  HASH_SALT_ROUNDS: Number,
};

export let env: Env;

export function loadEnv(): void {
  env = load(schema);
}
