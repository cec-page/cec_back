import multer from "multer";
import { Request, Response } from "express";
import University from "../models/University";
import NotFoundError from "../exception/NotFoundError";
import { getFileExtension } from "./utilityFunctions";
import { expressYupMiddleware } from "express-yup-middleware";
import * as Yup from "yup";

enum UploadTo {
  University = "university",
}

const storage = multer.diskStorage({
  destination: async function (req: Request, file, cb) {
    cb(null, file.fieldname + "-" + Date.now());
  },
  filename: async function (req, file, cb) {
    const uploadTo = req.params.uploadTo;
    const { targetId } = req.body;

    const ext = getFileExtension(file.filename);
    switch (uploadTo) {
      case UploadTo.University:
        const university = await University.findById(targetId);

        if (!university) {
          cb(new NotFoundError("Universidad no encontrar"), "");
          return;
        }

        cb(null, `upload/${university.slug}/logo${ext}`);

        break;

      default:
        cb(new NotFoundError("Upload target not supported"), "");
        break;
    }
  },
});

const upload = multer({ storage });

export const handleFileUpload = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            uploadTo: Yup.string().oneOf([...Object.values(UploadTo)]),
          }),
        },
      },
    },
  }),
  upload.single("file"),
  (req: Request, res: Response) => {
    res.status(201);
  },
];
