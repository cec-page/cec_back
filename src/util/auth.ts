import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import moment from "moment";
import { env } from "../env";

const JWT_PRIVATE_KEY = env.JWT_SECRET;
const SALT_ROUNDS = env.HASH_SALT_ROUNDS;

export const hashPassword = (plainText: string) => {
  return bcrypt.hash(plainText, SALT_ROUNDS);
};

export const checkPassword = (plainText: string, hash: string) => {
  return bcrypt.compare(plainText, hash);
};

export const createIdToken = (email: string, userId: string) => {
  const time = moment().utc();

  const tokenPayload: TokenPayload = {
    email: email,
    userId: userId,
    date: time,
  };

  const token = jwt.sign(tokenPayload, JWT_PRIVATE_KEY, {
    expiresIn: env.JWT_ID_DURATION,
  });
  return token;
};

export const verifyIdToken = (token: string) => {
  const payload = jwt.verify(token, JWT_PRIVATE_KEY) as TokenPayload;
  return payload;
};
