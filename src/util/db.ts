import mongoose from "mongoose";

export const connectDev = () =>
  mongoose.connect(
    "mongodb+srv://adrianjmejias:adri_123_123@development-lvhve.mongodb.net/cec_prod?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false }
  );

export const connectTest = () => {
  return mongoose
    .connect(
      "mongodb+srv://adrianjmejias:adri_123_123@development-lvhve.mongodb.net/cec_dev?retryWrites=true&w=majority",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      }
    )
    .catch((err) => console.error(err));
};

export const disconnectTest = () => {
  return mongoose.disconnect().catch((err) => console.error(err));
};
