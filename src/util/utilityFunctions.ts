import BadRequestError from "../exception/BadRequest";

export function getFileExtension(filename: string) {
  const ext = filename.split(".").pop();

  if (!ext) {
    throw new BadRequestError("File has no extension");
  }

  return ext;
}
