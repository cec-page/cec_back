import { loadEnv } from "./env";
loadEnv();

import app from "./app";
import { connectDev } from "./util/db";

connectDev()
  .then(() => {
    const PORT = Number.parseInt(process.env.PORT || "5000");
    app.listen(PORT, () => {
      console.log(`listening port ${PORT}`);
    });
  })
  .catch((err) => {
    console.error(`Couldn't connect with db ${err}`);
  });
