import cors from "cors";
import express, { Request, Response } from "express";
import helmet from "helmet";
import morgan from "morgan";
import ErrorHandler from "./exception/ErrorHandler";
import {
  CreateDepartment,
  DeleteDepartment,
  GetDepartment,
  UpdateDepartment,
} from "./handlers/Department";
import {
  CreateGuide,
  DeleteGuide,
  GetGuide,
  UpdateGuide,
} from "./handlers/Guide";
import {
  CreateSubject,
  GetSubject,
  GetSubjectsPaginated,
  UpdateSubject,
} from "./handlers/Subject";
import {
  CreateUniversity,
  DeleteUniversity,
  GetUniversity,
  GetUniversityBySlug,
  UpdateUniversity,
} from "./handlers/University";
import {
  AuthenticationWall,
  GetUser,
  SignIn,
  SignUp,
  UpdateUser,
} from "./handlers/User";
import {
  CheckIfUserIsSuscribed,
  SubjectsByUser,
  ToggleSuscription,
  UsersBySubject,
} from "./models/Suscription";
import { handleFileUpload } from "./util/middlewares";

const PORT = Number.parseInt(process.env.PORT || "5000");
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  morgan(":method :url :status :response-time ms - :res[content-length]")
);
app.use(helmet());

// Auth Routes
app.post("/sign-in", SignIn);
app.post("/sign-Up", SignUp);

// User
app.get("/user/:id", GetUser);
app.put("/user/:id", AuthenticationWall, UpdateUser);

// Subject o Materia
app.post("/subject", AuthenticationWall, CreateSubject);
app.get("/subject/:id", GetSubject);
app.get("/subject", GetSubjectsPaginated);
app.put("/subject/:id", AuthenticationWall, UpdateSubject);

// Suscription
app.get("/users-by-subject/:subjectId", UsersBySubject);
app.get("/subjects-by-user/:userId", SubjectsByUser);
app.put(
  "/suscription/:userId/:subjectId",
  AuthenticationWall,
  ToggleSuscription
);
app.get("/suscription/:userId/:subjectId", CheckIfUserIsSuscribed);

// University
app.post("/university", AuthenticationWall, CreateUniversity);
app.get("/university/:id", GetUniversity);
app.get("/university-slug/:slug", GetUniversityBySlug);
app.put("/university/:id", AuthenticationWall, UpdateUniversity);
app.delete("/university/:id", AuthenticationWall, DeleteUniversity);

// Department
app.post("/department", AuthenticationWall, CreateDepartment);
app.get("/department/:id", GetDepartment);
app.put("/department/:id", AuthenticationWall, UpdateDepartment);
app.delete("/department/:id", AuthenticationWall, DeleteDepartment);

// Guides
app.post("/upload/:uploadTo", handleFileUpload);
app.post("/guides", CreateGuide);
app.get("/guides/:id", GetGuide);
app.put("/guides/:id", UpdateGuide);
app.delete("/guides/:id", DeleteGuide);

// Fallbacks
app.get("/", (req: Request, res: Response) => {
  res.status(200).json(`listening port ${PORT}`);
});

app.use("*", (req: Request, res: Response) => {
  res.status(404).json({ message: "Endpoint not found" });
});

app.use(ErrorHandler);

export default app;
