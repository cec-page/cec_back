import { StatusCodes } from "http-status-codes";
import BaseError from "./BaseError";

export default class AlreadyExistsError extends BaseError {
  constructor() {
    super(StatusCodes.CONFLICT, "Correo en uso.");
  }
}
