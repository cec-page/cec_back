import { StatusCodes } from "http-status-codes";

export default class BaseError extends Error {
  status: number;
  message: string;
  constructor(status: number, message: string) {
    super(message);
    this.status = status || StatusCodes.INTERNAL_SERVER_ERROR;
    this.message = message;
  }
}
