import BaseError from "./BaseError";
import { StatusCodes } from "http-status-codes";

export default class NotFoundError extends BaseError {
  constructor(msg: string = "No encontrado") {
    super(StatusCodes.NOT_FOUND, msg);
  }
}
