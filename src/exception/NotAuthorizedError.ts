import BaseError from "./BaseError";
import { StatusCodes } from "http-status-codes";

export default class NotAuthorizedError extends BaseError {
  constructor(msg: string = "No autorizado.") {
    super(StatusCodes.UNAUTHORIZED, msg);
  }
}
