import BaseError from "./BaseError";
import { StatusCodes } from "http-status-codes";

export default class BadRequestError extends BaseError {
  constructor(msg: string = "Bad request") {
    super(StatusCodes.BAD_REQUEST, msg);
  }
}
