import BaseError from "./BaseError";
import { Response, Request, NextFunction } from "express";
import { StatusCodes } from "http-status-codes";

const ErrorHandler = (
  error: BaseError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res
    .status(error.status || StatusCodes.NOT_FOUND)
    .json({ error: error.message || "Endpoint not supported." });
  next();
};
export default ErrorHandler;
