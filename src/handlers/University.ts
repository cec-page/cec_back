import { Request, Response, NextFunction } from "express";
import { expressYupMiddleware } from "express-yup-middleware";
import * as Yup from "yup";
import { StatusCodes } from "http-status-codes";
import NotFoundError from "../exception/NotFoundError";
import University, { IUniversity } from "../models/University";

export const CreateUniversity = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            name: Yup.string().required(),
            description: Yup.string().required(),
            slug: Yup.string().required(),
          }),
        },
      },
    },
  }),
  async (
    req: Request<{}, {}, IUniversity>,
    res: Response,
    next: NextFunction
  ) => {
    const { name, description, departments, slug } = req.body;

    try {
      const university = await University.create({
        name,
        description,
        slug,
        departments: [],
        subjects: [],
      });
      return res.status(StatusCodes.CREATED).json(university);
    } catch (err) {
      next(err);
      return;
    }
  },
];

export const GetUniversity = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    University.findById(req.params.id)
      .exec()
      .then((university) => {
        return res.status(StatusCodes.OK).json(university);
      })
      .catch(next);
  },
];

export const GetUniversityBySlug = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            slug: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ slug: string }>, res: Response, next: NextFunction) => {
    University.findOne({ slug: req.params.slug })
      .exec()
      .then((university) => {
        if (!university) {
          next(new NotFoundError("University slug not found"));
          return;
        }

        return res.status(StatusCodes.OK).json(university);
      })
      .catch(next);
  },
];

export const UpdateUniversity = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    const { name, description, img, majors } = req.body;

    University.findById(req.params.id, { new: true })
      .exec()
      .then(async (university) => {
        if (!university) {
          next(new NotFoundError());
          return;
        }
        university.name = name;
        university.description = description;
        await university.save();
        res.status(StatusCodes.OK).json(university);
        return;
      })
      .catch(next);
  },
];

export const DeleteUniversity = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    University.findByIdAndDelete({ _id: req.params.id })
      .exec()
      .then((university) => {
        if (!university) {
          return res.status(StatusCodes.OK).json(false);
        }
        res.status(StatusCodes.OK).json(true);
        return;
      })
      .catch(next);
  },
];
