import { Request, Response, NextFunction } from "express";
import { expressYupMiddleware } from "express-yup-middleware";
import * as Yup from "yup";
import { StatusCodes } from "http-status-codes";
import NotFoundError from "../exception/NotFoundError";
import Department from "../models/Department";
import Subject, { ISubject } from "../models/Subject";

export const CreateSubject = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            name: Yup.string().required(),
            description: Yup.string().required(),
            departmentId: Yup.string().required(),
            slug: Yup.string().required(),
            university: Yup.string().required(),
          }),
        },
      },
    },
  }),
  async (req: Request<{}, {}, ISubject>, res: Response, next: NextFunction) => {
    const { name, description, departmentId, guides, university } = req.body;
    let { slug } = req.body;

    try {
      const department = await Department.findById(departmentId);
      if (!department) {
        next(new NotFoundError("Departamento no encontrado"));
        return;
      }
      slug = department.slug + "/" + slug;
      const createdSubject = await Subject.create({
        name,
        description,
        departmentId,
        guides,
        slug,
        university,
      });
      department.subjects.push(createdSubject._id);
      await department.save();
      return res.status(StatusCodes.CREATED).json(createdSubject);
    } catch (err) {
      next(err);
    }
  },
];

export const GetSubject = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    Subject.findById(req.params.id)
      .exec()
      .then((subject) => {
        return res.status(StatusCodes.OK).json(subject);
      })
      .catch(next);
  },
];

export const GetSubjectsPaginated = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            // id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    Subject.find()
      .exec()
      .then((subjects) => {
        return res.status(StatusCodes.OK).json(subjects);
      })
      .catch(next);
  },
];

export const UpdateSubject = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    const { name, description } = req.body;
    const data = { name, description };
    Subject.findByIdAndUpdate(req.params.id, data, { new: true })
      .exec()
      .then((subject) => {
        if (!subject) {
          next(new NotFoundError("Materia no encontrada"));
          return;
        }
        subject.name = name;
        subject.description = description;

        return res.status(StatusCodes.OK).json(subject);
      })
      .catch(next);
  },
];
