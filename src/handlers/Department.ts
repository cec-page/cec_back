import { Request, Response, NextFunction } from "express";
import { expressYupMiddleware } from "express-yup-middleware";
import * as Yup from "yup";
import { StatusCodes } from "http-status-codes";
import NotFoundError from "../exception/NotFoundError";
import Department, { IDepartment } from "../models/Department";
import University from "../models/University";

export const CreateDepartment = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            name: Yup.string().required(),
            description: Yup.string().required(),
            universityId: Yup.string().required(),
            slug: Yup.string().required(),
          }),
        },
      },
    },
  }),
  async (
    req: Request<{}, {}, IDepartment>,
    res: Response,
    next: NextFunction
  ) => {
    const { name, description, universityId } = req.body;
    let { slug } = req.body;
    try {
      const tempUniv = await University.findById(universityId);

      if (!tempUniv) {
        next(new NotFoundError("Universidad No Encontrada"));
        return;
      }

      slug = tempUniv.slug + "/" + slug;

      const department = await Department.create({
        name,
        description,
        universityId,
        slug,
        subjects: [],
      });
      tempUniv.departments.push(department._id);
      await tempUniv.save();

      res.status(StatusCodes.CREATED).json(department);
      return;
    } catch (err) {
      next(err);
      return;
    }
  },
];

export const GetDepartment = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    Department.findById(req.params.id)
      .exec()
      .then((product) => {
        res.status(StatusCodes.OK).json(product);
        return;
      })
      .catch(next);
  },
];

export const UpdateDepartment = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    const { name, description } = req.body;
    const data = { name, description };
    Department.findByIdAndUpdate(req.params.id, data, { new: true })
      .exec()
      .then((department) => {
        if (!department) {
          next(new NotFoundError("Departamento no encontrado"));
          return;
        }

        res.status(StatusCodes.OK).json(department);
        return;
      })
      .catch(next);
  },
];

export const DeleteDepartment = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    Department.findByIdAndDelete({ _id: req.params.id })
      .exec()
      .then((department) => {
        if (!department) {
          return res.status(StatusCodes.OK).json(false);
        }

        res.status(StatusCodes.OK).json(true);
        return;
      })
      .catch(next);
  },
];
