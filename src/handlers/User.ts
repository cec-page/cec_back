import { NextFunction, Request, Response } from "express";
import { expressYupMiddleware } from "express-yup-middleware";
import { StatusCodes } from "http-status-codes";
import * as Yup from "yup";
import {
  AlreadyExistsError,
  BadRequestError,
  NotAuthorizedError,
  NotFoundError,
} from "../exception";
import User, { IUser } from "../models/User";
import { checkPassword, createIdToken, verifyIdToken } from "../util/auth";
import { emailValidationYup, passwordValidationYup } from "../util/validations";

export const SignUp = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            name: Yup.string().required(),
            lastname: Yup.string().required(),
            userName: Yup.string().required(),
            password: passwordValidationYup,
            email: emailValidationYup,
          }),
        },
      },
    },
  }),
  async (req: Request<{}, {}, IUser>, res: Response, next: NextFunction) => {
    const {
      email,
      name,
      lastname,
      password,
      belongsToDepartment,
      userName,
    } = req.body;
    const user = await User.findOne({ email });

    if (user) {
      next(new AlreadyExistsError());
      return;
    }

    User.create({
      email,
      name,
      lastname,
      password,
      belongsToDepartment,
      userName,
    })
      .then((userCreated) => {
        const token = createIdToken(userCreated.email, userCreated._id);
        res.status(StatusCodes.CREATED).json({
          user: userCreated,
          token,
        });
      })
      .catch(next);
    return;
  },
];

export const SignIn = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            password: passwordValidationYup,
            email: emailValidationYup,
          }),
        },
      },
    },
  }),
  async (
    req: Request<{}, {}, { password: string; email: string }>,
    res: Response,
    next: NextFunction
  ) => {
    const data = req.body;
    const user = await User.findOne({ email: data.email });
    if (!user) {
      next(new NotFoundError("Usuario no existe"));
      return;
    }

    checkPassword(data.password, user?.password)
      .then((isPasswordCorrect) => {
        if (!isPasswordCorrect) {
          next(new NotAuthorizedError("Contraseña invalida"));
          return;
        }
        const token = createIdToken(user.email, user._id);
        return res.status(StatusCodes.OK).json({
          user,
          token,
        });
      })
      .catch(next);
  },
];

export const GetUser = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  async (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    User.findById(req.params.id)
      .exec()
      .then((user) => {
        if (!user) {
          next(new NotFoundError("Usuario no encontrado."));
          return;
        }
        return res.status(StatusCodes.OK).json(user);
      })
      .catch(next);
  },
];

export const GetUserBySlug = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            userName: Yup.string().required(),
          }),
        },
      },
    },
  }),
  async (
    req: Request<{ userName: string }>,
    res: Response,
    next: NextFunction
  ) => {
    const userName = req.body;
    User.findOne({ userName })
      .exec()
      .then((user) => {
        if (!user) {
          next(new NotFoundError("Usuario con username no encontrado."));
          return;
        }
        return res.status(StatusCodes.OK).json(user);
      })
      .catch(next);
  },
];

export const UpdateUser = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            name: Yup.string().required(),
            email: emailValidationYup,
          }),
        },
      },
    },
  }),
  async (
    req: Request<{ id: string }, {}, IUser>,
    res: Response,
    next: NextFunction
  ) => {
    const { email, name, lastname, belongsToDepartment } = req.body;
    const data = { email, name, lastname, belongsToDepartment };

    User.findByIdAndUpdate(req.params.id, data, { new: true })
      .then((user) => {
        if (!user) {
          next(new NotFoundError("Usuario no encontrado."));
          return;
        }
        return res.status(StatusCodes.OK).json(user);
      })
      .catch(next);
    return;
  },
];

export const AuthenticationWall = [
  expressYupMiddleware({
    propertiesToValidate: ["headers"],
    schemaValidator: {
      schema: {
        headers: {
          yupSchema: Yup.object().shape({
            authorization: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request, res: Response, next: NextFunction) => {
    const token = req.headers.authorization as string;
    const parts = token.split(" ");

    if ((false && parts.length !== 2) || parts[0] !== "Bearer") {
      next(new BadRequestError("Malformed header"));
      return;
    }

    // let payload;
    // try {
    //   payload = verifyIdToken(parts[1]);
    // } catch (err) {
    //   next(new BadRequestError(err.message));
    //   return;
    // }

    // req.user = payload;
    req.userId = "5f77f28f244ab00a84f156e4"; // payload.userId;
    next();
  },
];
