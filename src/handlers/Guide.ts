import { Request, Response, NextFunction } from "express";
import { expressYupMiddleware } from "express-yup-middleware";
import * as Yup from "yup";
import { StatusCodes } from "http-status-codes";
import NotFoundError from "../exception/NotFoundError";
import moment from "moment";
import Guide, { guideTypeValidator, IGuide } from "../models/Guide";

export const CreateGuide = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        body: {
          yupSchema: Yup.object().shape({
            name: Yup.string().required(),
            type: guideTypeValidator,
            semester: Yup.string().required(),
            author: Yup.string().required(),
            rating: Yup.number().notRequired(),
            description: Yup.string().required(),
          }),
        },
      },
    },
  }),
  // receiveFileUpload("./images", "file", (file) => file.originalname),
  async (req: Request<{}, {}, IGuide>, res: Response) => {
    const { name, description, type, semester, author, rating } = req.body;
    const uploadDate = moment().utc().toDate();

    const guide = await Guide.create({
      name,
      description,
      type,
      semester,
      author,
      rating,
      uploadDate,
      uploadedBy: req.userId,
    });
    return res.status(StatusCodes.CREATED).json(guide);
  },
];

export const GetGuide = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    Guide.findById(req.params.id)
      .exec()
      .then((guide) => {
        return res.status(StatusCodes.OK).json(guide);
      })
      .catch(next);
  },
];

export const UpdateGuide = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    const { name, description, type, semester, author } = req.body;
    const data = { name, description, type, semester, author };
    Guide.findByIdAndUpdate(req.params.id, data, { new: true })
      .exec()
      .then((guide) => {
        if (!guide) {
          next(new NotFoundError());
          return;
        }
        res.status(StatusCodes.OK).json(guide);
        return;
      })
      .catch(next);
  },
];

export const DeleteGuide = [
  expressYupMiddleware({
    schemaValidator: {
      schema: {
        params: {
          yupSchema: Yup.object().shape({
            id: Yup.string().required(),
          }),
        },
      },
    },
  }),
  (req: Request<{ id: string }>, res: Response, next: NextFunction) => {
    Guide.findByIdAndDelete({ _id: req.params.id })
      .exec()
      .then((guide) => {
        if (!guide) {
          return res.status(StatusCodes.OK).json(false);
        }
        res.status(StatusCodes.OK).json(true);
        return;
      })
      .catch(next);
  },
];
