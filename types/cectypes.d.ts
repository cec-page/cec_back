// // https://stackoverflow.com/questions/40456231/typescript-module-augmentation-overwrites-the-original-module
// import { Request } from "express-serve-static-core";

// declare module "express-serve-static-core" {
//   export interface Request extends Express.Request {
//     user: any;
//     userId: string;
//   }
// }

export {};

declare global {
  /*~ Here, declare things that go in the global namespace, or augment
   *~ existing declarations in the global namespace
   */
  interface TokenPayload {
    userId: string;
    email: string;
    date: moment.Moment;
  }
}
