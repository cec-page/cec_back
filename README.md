# Pasos para correr el proyecto

## Pre-requisitos
  * [Debes tener instalado nodejs](https://nodejs.org/en/download/)
  * [Debes tener instalado yarn globalmente](https://yarnpkg.com/getting-started/install)


## Una vez cumplas con los requisitos.

1. Entrar en la carpeta del proyecto usando la consola
2. Ejecutar el comando `yarn` para instalar las dependencias
3. Ejecutar el comando `yarn dev` para correr el proyecto
4. El proyecto debe estar ejecutandose en localhost:3000

## Notas importantes

Para poder desarrollar nos vamos a enfocar en el uso de git. 
* Cada tarea que se haga debe estar respaldada con un issue.
* Cada push a develop debe estar respaldado por un merge request. Los mantainers deben hacer code-review y aprobar.
* Si trabajan en una tarea traten de hacer rebase con develop constantemente para que tengan siempre los ultimos cambios.
