import app from "../../src/app";
import request from "supertest";
import { connectTest, disconnectTest } from "../../src/util/db";
import { StatusCodes } from "http-status-codes";
import Department from "../../src/models/Department";

let departmentId: string;

const deleteDepartment = async (depId: string | undefined) => {
  if (depId) {
    Department.findByIdAndDelete(depId)
      .exec()
      .catch((err) => console.error(err));
  }
};

describe("Testing department endpoints", () => {
  beforeAll(async () => {
    await connectTest();
  });

  afterAll(async () => {
    await disconnectTest();
  });

  describe("Department/CreateDepartment", () => {
    beforeAll(async () => {
      await Department.findOneAndDelete({ name: "testName" });
    });

    test("Fails. Department already exists.", async () => {
      const department = await request(app).post("/department").send({
        name: "testName",
        description: "Lorem Ipsum",
        subjects: [],
      });

      console.log(department.body.errors.body);
      const depId = department.body.payload._id;
      try {
        await request(app).post("/department").send({
          name: "testName",
          description: "Lorem Ipsum",
          subjects: [],
        });
      } catch (error) {
        expect(error.status).toBe(StatusCodes.CONFLICT);
      }
      await deleteDepartment(depId);
    });

    test("Passes.  Create Department.", () => {
      return request(app)
        .post("/department")
        .send({
          name: "testName",
          description: "Lorem Ipsum",
          subjects: [],
        })
        .then(async (res) => {
          expect(res.status).toBe(StatusCodes.CREATED);
          await deleteDepartment(res.body.payload.department.id);
        });
    });
  });

  describe("Department/GetDepartment", () => {
    beforeAll(async () => {
      const department = await Department.create({
        name: "testName",
        description: "Loreem Ipsum",
        subjects: [],
      });
      departmentId = department._id;
    });

    afterAll(async () => {
      await Department.findOneAndDelete({ name: "testName" });
    });

    test("Passes. Gets Department", async () => {
      try {
        const department = await request(app)
          .get("/department" + departmentId)
          .send();
      } catch (error) {
        expect(error.status).toBe(StatusCodes.OK);
      }
    });
    test("Fails. Department doesn't exist", async () => {
      try {
        const department = await request(app)
          .get("/department/qweqweqwe12123")
          .send();
      } catch (error) {
        expect(error.status).toBe(StatusCodes.NOT_FOUND);
      }
    });
  });
});
