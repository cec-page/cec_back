import app from "../../src/app";
import request from "supertest";
import { connectTest, disconnectTest } from "../../src/util/db";
import { StatusCodes } from "http-status-codes";
import User from "../../src/models/User";

const deleteUser = async (userId: string | undefined) => {
  if (userId) {
    User.findByIdAndDelete(userId)
      .exec()
      .catch((err) => console.error(err));
  }
};

describe("Testing user endpoints", () => {
  beforeAll(async () => {
    await connectTest();
  });

  afterAll(async () => {
    await disconnectTest();
  });

  describe("User/SignUp", () => {
    beforeAll(async () => {
      await User.findOneAndDelete({ email: "pepe@test.com" });
    });
    const PasswordTest = (
      testName: string,
      passwordValue: string,
      emailValue: string
    ) => {
      test(testName, () => {
        return request(app)
          .post("/sign-up")
          .send({
            name: "pepe",
            email: emailValue,
            password: passwordValue,
          })
          .catch(async (res) => {
            await deleteUser(res.body.payload.user.id);
            expect(res.status).toBe(StatusCodes.BAD_REQUEST);
          });
      });
    };
    PasswordTest("Fails. Invalid email.", "12345Test_", "correomalo");
    PasswordTest(
      "Fails. Password no lowercase.",
      "12345TEST_",
      "pepe@test.com"
    );
    PasswordTest(
      "Fails. Password no uppercase.",
      "12345test_",
      "pepe@test.com"
    );
    PasswordTest("Fails. Password no simbol.", "12345test", "pepe@test.com");
    PasswordTest("Fails. Password no number.", "ttasTest_", "pepe@test.com");
    PasswordTest(
      "Fails. Password too long ( 15 > ).",
      "12345TestazoGigante123132123_!_!_!_!_",
      "pepe@test.com"
    );
    PasswordTest(
      "Fails. Password too short ( 6 < ).",
      "12Ae!",
      "pepe@test.com"
    );

    test("Fails email already used.", async () => {
      const user = await request(app).post("/sign-up").send({
        name: "pepe",
        email: "pepe@test.com",
        password: "12345Test_",
      });

      const userId = user.body.payload.user._id;
      try {
        await request(app).post("/sign-up").send({
          name: "pepe",
          email: "pepe@test.com",
          password: "12345Test_",
        });
      } catch (error) {
        expect(error.status).toBe(StatusCodes.CONFLICT);
      }
      await deleteUser(userId);
    });

    test("Passes.", () => {
      return request(app)
        .post("/sign-up")
        .send({
          name: "pepe",
          email: "pepe@test.com",
          password: "12345Test_",
        })
        .then(async (res) => {
          expect(res.status).toBe(StatusCodes.CREATED);
          await deleteUser(res.body.payload.user.id);
        });
    });
  });

  describe("User/SignIn", () => {
    beforeAll(async () => {
      await User.create({
        email: "pepe@test.com",
        password: "12345Test_",
        name: "Pepe",
        lastname: "Gonzales",
      });
    });

    afterAll(async () => {
      await User.findOneAndDelete({ email: "pepe@test.com" });
    });

    test("Fails. Email doesn't exist", async () => {
      try {
        const user = await request(app).post("/sign-in").send({
          email: "correoinexistente@test.com",
          password: "12345Test_",
        });
      } catch (error) {
        expect(error.status).toBe(StatusCodes.NOT_FOUND);
      }
    });

    test("Fails. Email exists, password wrong", async () => {
      try {
        const user = await request(app).post("/sign-in").send({
          email: "pepe@test.com",
          password: "12345Test_!",
        });
      } catch (error) {
        expect(error.status).toBe(StatusCodes.UNAUTHORIZED);
      }
    });

    test("Passes.", async () => {
      try {
        const user = await request(app).post("/sign-in").send({
          email: "pepe@test.com",
          password: "12345Test_",
        });
        await deleteUser(user.body.payload.user._id);
      } catch (error) {
        expect(error.status).toBe(StatusCodes.OK);
      }
    });
  });
});
